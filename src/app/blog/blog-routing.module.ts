import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogListComponent } from './blog-list/blog-list.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { LeftsidebarComponent } from './leftsidebar/leftsidebar.component';
import { RightsidebarComponent } from './rightsidebar/rightsidebar.component';
import { DetailLeftsidebarComponent} from './detail-leftsidebar/detail-leftsidebar.component';
import { DetailRightsidebarComponent} from './detail-rightsidebar/detail-rightsidebar.component';
  import { from } from 'rxjs';

const routes: Routes = [
  {
    path: '',
    //component: BlogListComponent,

    children: [
      {
        path: 'list',
        component: BlogListComponent,
      },
      {
        path: 'details',
        component: BlogDetailComponent,
      }, 
      {
        path: 'left-sidebar',
        component: LeftsidebarComponent,
      }, 
      {
        path: 'right-sidebar',
        component: RightsidebarComponent,
      }, 
      {
        path: 'details-left-sidebar',
        component: DetailLeftsidebarComponent,
      },
      {
        path: 'details-right-sidebar',
        component: DetailRightsidebarComponent,
      }         
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
