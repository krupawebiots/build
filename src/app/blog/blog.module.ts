import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {SharedModule } from '../shared/shared.module';
import { BlogRoutingModule } from './blog-routing.module';
import { BlogListComponent } from './blog-list/blog-list.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { BlogSidebarComponent } from './blog-sidebar/blog-sidebar.component';
import { DetailLeftsidebarComponent } from './detail-leftsidebar/detail-leftsidebar.component';
import { DetailRightsidebarComponent } from './detail-rightsidebar/detail-rightsidebar.component';
import { RightsidebarComponent } from './rightsidebar/rightsidebar.component';
import { LeftsidebarComponent } from './leftsidebar/leftsidebar.component';
import {RouterModule } from '@angular/router';
import { MenuComponent } from '../shared/header/widgets/menu/menu.component';

@NgModule({
 
  declarations: [
    BlogListComponent, 
    BlogDetailComponent, 
    BlogSidebarComponent, 
    DetailLeftsidebarComponent, 
    DetailRightsidebarComponent, 
    RightsidebarComponent, 
    LeftsidebarComponent, 
    
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    SharedModule,
    RouterModule
  ]
})
export class BlogModule { }
