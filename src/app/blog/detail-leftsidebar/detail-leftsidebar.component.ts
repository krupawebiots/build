import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detail-leftsidebar',
  templateUrl: './detail-leftsidebar.component.html',
  styleUrls: ['./detail-leftsidebar.component.scss']
})
export class DetailLeftsidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    console.log("blog detail leftside bar call");
  }

}
