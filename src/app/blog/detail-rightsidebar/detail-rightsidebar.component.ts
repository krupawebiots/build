import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detail-rightsidebar',
  templateUrl: './detail-rightsidebar.component.html',
  styleUrls: ['./detail-rightsidebar.component.scss']
})
export class DetailRightsidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    console.log("blog detail rightsidebar call");
  }

}
