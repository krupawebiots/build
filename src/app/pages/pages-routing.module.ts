import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { ServiceComponent} from './service/service.component';
import { TeamComponent} from './team/team.component';
import { ContactusComponent } from './contactus/contactus.component';
import { ErrorPageComponent} from './error-page/error-page.component';
import { ComingSoonComponent } from './coming-soon/coming-soon.component';

const routes: Routes = [
  {
    path: 'about',
    component: AboutComponent,
  },
  {
    path: 'service',
    component: ServiceComponent,
  }, 
  {
    path: 'team',
    component: TeamComponent,
  }, 
  {
    path: 'contact-us',
    component: ContactusComponent,
  }, 
  {
    path: 'coming-soon',
    component:ComingSoonComponent,
  },
  {
    path: 'error-page',
    component: ErrorPageComponent,
  }         
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
