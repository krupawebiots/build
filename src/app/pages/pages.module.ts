import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule } from '@angular/router';
import {SharedModule} from '../shared/shared.module';

import { PagesRoutingModule } from './pages-routing.module';
import { ContactusComponent } from './contactus/contactus.component';
import { ComingSoonComponent } from './coming-soon/coming-soon.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { ServiceComponent } from './service/service.component';
import { AboutComponent } from './about/about.component';
import { TeamComponent } from './team/team.component';

@NgModule({
  exports:[ContactusComponent, ComingSoonComponent, ErrorPageComponent, ServiceComponent, AboutComponent, TeamComponent],
  declarations: [ ContactusComponent, ComingSoonComponent, ErrorPageComponent, ServiceComponent, AboutComponent, TeamComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    SharedModule,
    RouterModule
  ]
})
export class PagesModule { }
