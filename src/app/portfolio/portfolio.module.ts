import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {SharedModule} from '../shared/shared.module';

import { PortfolioRoutingModule } from './portfolio-routing.module';
import { PortfolioComponent } from './portfolio.component';
import { SinglePortfolioComponent } from './single-portfolio/single-portfolio.component';
import { PortfolioThreeGridComponent } from './portfolio-three-grid/portfolio-three-grid.component';
import { PortfolioFourGridComponent } from './portfolio-four-grid/portfolio-four-grid.component';
import { MasonaryThreeGridComponent } from './masonary-three-grid/masonary-three-grid.component';
import { MasonaryFourGridComponent } from './masonary-four-grid/masonary-four-grid.component';
import { MasonaryFullwidthComponent } from './masonary-fullwidth/masonary-fullwidth.component';
import { PortfolioManuComponent } from './portfolio-manu/portfolio-manu.component';
// import { isPromise } from 'q';

@NgModule({
  exports:[PortfolioComponent],
  declarations: [PortfolioComponent, SinglePortfolioComponent, PortfolioThreeGridComponent, PortfolioFourGridComponent, MasonaryThreeGridComponent, MasonaryFourGridComponent, MasonaryFullwidthComponent, PortfolioManuComponent],
  imports: [
    CommonModule,
    PortfolioRoutingModule,
    RouterModule,
    SharedModule
  ]
})
export class PortfolioModule { }
