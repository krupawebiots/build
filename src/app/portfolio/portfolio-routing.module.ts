import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SinglePortfolioComponent } from './single-portfolio/single-portfolio.component';
import { PortfolioThreeGridComponent } from './portfolio-three-grid/portfolio-three-grid.component'
import { PortfolioFourGridComponent } from  './portfolio-four-grid/portfolio-four-grid.component';
import { MasonaryThreeGridComponent} from './masonary-three-grid/masonary-three-grid.component';
import { MasonaryFourGridComponent } from './masonary-four-grid/masonary-four-grid.component';
import {MasonaryFullwidthComponent } from './masonary-fullwidth/masonary-fullwidth.component';
const routes: Routes = [
  {
    path: '',
    //component: BlogListComponent,

    children: [
      {
        path: 'single-portfolio',
        component: SinglePortfolioComponent,
      },
      {
        path: 'portfolio-three',
        component: PortfolioThreeGridComponent,
      }, 
      {
        path: 'portfolio-four',
        component: PortfolioFourGridComponent,
      }, 
      {
        path: 'masonary-three',
        component: MasonaryThreeGridComponent,
      }, 
      {
        path: 'masonary-four',
        component: MasonaryFourGridComponent,
      },
      {
        path: 'masonary-fullwidth',
        component: MasonaryFullwidthComponent,
      }         
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortfolioRoutingModule { }
