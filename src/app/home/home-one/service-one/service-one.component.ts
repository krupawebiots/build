import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-service-one',
  templateUrl: './service-one.component.html',
  styleUrls: ['./service-one.component.scss']
})
export class ServiceOneComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public data = [{
    
    img: '/assets/images/construction_2/services/1.jpg',
    heading:'Building Information',
    icon:'assets/images/construction_6/service/6.png',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read more'
  }, 
  {
    img: 'assets/images/construction_2/services/2.jpg',
    heading:'Contraction Services',
    icon:'assets/images/construction_6/service/5.png',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read more'
  }, 
  {
    img: 'assets/images/construction_2/services/3.jpg',
    heading:'Pre-Contraction',
    icon:'assets/images/construction_6/service/4.png',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read more'
  }, 
  {
    img: 'assets/images/construction_2/services/4.jpg',
    heading:'Green Building',
    icon:'assets/images/construction_6/service/3.png',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read more'
  }, 
  {
    img: 'assets/images/construction_2/services/5.jpg',
    heading:'Construction Design',
    icon:'assets/images/construction_6/service/2.png',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read more'
  }, 
  {
    img: 'assets/images/construction_2/services/4.jpg',
    heading:'Interior Design',
    icon:'assets/images/construction_6/service/1.png',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read more'
  }]


}
