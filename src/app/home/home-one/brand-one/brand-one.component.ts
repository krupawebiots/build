import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-brand-one',
  templateUrl: './brand-one.component.html',
  styleUrls: ['./brand-one.component.scss']
})
export class BrandOneComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  datas=[
    {
      img:'assets/images/brand/1.png'
    },
    {
      img:'assets/images/brand/2.png'
    },
    {
      img:'assets/images/brand/3.png'
    },
    
    {
      img:'assets/images/brand/4.png'
    },
    {
      img:'assets/images/brand/5.png'
    }
  ];

  
  myslideroption={
    autoplay: true,
    loop: true,
    dots: false,
    margin: 30,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    items: 5,
    responsive:{
        0:{
            items:1
        },
        481:{
            items:2
        },
        600:{
            items:2
        },
        768:{
            items:3
        },
        992:{
            items:4
        },
        1000:{
            items:5
        }
    }
  };
}
