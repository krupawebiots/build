import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider-six',
  templateUrl: './slider-six.component.html',
  styleUrls: ['./slider-six.component.scss']
})
export class SliderSixComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  mySlideOptions={
    autoplay: false,
    center: true,
    loop: true,
    dots: false,
    nav: true,
    navText: ["<span class='icon icon-arrow-left7'></span>","<span class='icon icon-arrow-right7'></span>"],
    items: 1
  };

}
