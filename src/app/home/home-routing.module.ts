import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeOneComponent } from '../home/home-one/home-one.component';
import { HomeThreeComponent } from '../home/home-three/home-three.component';
import { HomeSixComponent } from '../home/home-six/home-six.component';
import { HomeTwoComponent } from '../home/home-two/home-two.component';
const routes: Routes = [
  { 
    path: 'one',
    component: HomeOneComponent
  },
  { 
    path: 'two',
    component: HomeTwoComponent
  },
  { 
    path: 'three',
    component: HomeThreeComponent
  },
  { 
    path: 'six',
    component: HomeSixComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
