import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-service-two',
  templateUrl: './service-two.component.html',
  styleUrls: ['./service-two.component.scss']
})
export class ServiceTwoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  public data = [{
    img: '/assets/images/construction_2/services/1.jpg',
    heading:'Building Information',
    icon:'assets/images/construction_2/service_icon/6.png',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read more'
  }, 
  {
    img: 'assets/images/construction_2/services/2.jpg',
    heading:'Contraction Servicesn',
    icon:'assets/images/construction_2/service_icon/5.png',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read more'
  }, 
  {
    img: 'assets/images/construction_2/services/3.jpg',
    heading:'Pre-Contraction',
    icon:'assets/images/construction_2/service_icon/4.png',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read more'
  }, 
  {
    img: 'assets/images/construction_2/services/4.jpg',
    heading:'Green Building',
    icon:'assets/images/construction_2/service_icon/3.png',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read more'
  }, 
  {
    img: 'assets/images/construction_2/services/5.jpg',
    heading:'Construction Design',
    icon:'assets/images/construction_2/service_icon/2.png',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read more'
  }, 
  {
    img: 'assets/images/construction_2/services/4.jpg',
    heading:'Interior Design',
    icon:'assets/images/construction_2/service_icon/1.png',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    link:'Read more'
  }]

}
