import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-testimonial-two',
  templateUrl: './testimonial-two.component.html',
  styleUrls: ['./testimonial-two.component.scss']
})
export class TestimonialTwoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public datas=[{
    img:'assets/images/testimonial/1.png',
    review:'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, consectetur adipisicing nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons Ut enim ad minim veniam.',
    name:'Ethen Mark',
    designation:'Director'
  },
  {
    img:'assets/images/testimonial/2.png',
    review:'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, consectetur adipisicing nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons Ut enim ad minim veniam.',
    name:'Ethen Mark',
    designation:'Director'
  },
  {
    img:'assets/images/testimonial/3.png',
    review:'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, consectetur adipisicing nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons Ut enim ad minim veniam.',
    name:'Ethen Mark',
    designation:'Director'
  }];

  myslideroptions=
  {
    autoplay: true,
    center: true,
    loop: true,
    dots: false,
    items: 1
  };

}
