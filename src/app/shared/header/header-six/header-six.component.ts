import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { WINDOW } from '../../services/windows.service';
declare var $: any;
@Component({
  selector: 'app-header-six',
  templateUrl: './header-six.component.html',
  styleUrls: ['./header-six.component.scss']
})
export class HeaderSixComponent implements OnInit {
  public fixedtop : boolean = false;
  constructor(
    @Inject(WINDOW) private window
    ) { }

  ngOnInit() {
  }

  // @HostListener Decorator    
  @HostListener("window:scroll", ['$event'])
  onWindowScroll(event) {
    let number = this.window.pageYOffset;
    if (number >= 200) { 
      this.fixedtop = true;
    } else {
      this.fixedtop = false;
    }
  }
}
