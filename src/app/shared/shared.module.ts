import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer/footer.component';
import { NewsletterComponent } from './footer/widgets/newsletter/newsletter.component';
import { SocialtagComponent } from './footer/widgets/socialtag/socialtag.component';
import { ServiceComponent } from './footer/widgets/service/service.component';
import { GalleryComponent } from './footer/widgets/gallery/gallery.component';
import { InformationComponent } from './footer/widgets/information/information.component';
import { CopyrightsComponent } from './footer/widgets/copyrights/copyrights.component';
import { MenuComponent } from './header/widgets/menu/menu.component';
import { TopbarComponent } from './header/widgets/topbar/topbar.component';
import { HeaderSixComponent } from './header/header-six/header-six.component';
import { RouterModule } from "@angular/router";
import { WINDOW_PROVIDERS } from './services/windows.service';
import { HeaderTwoComponent } from './header/header-two/header-two.component';
import { HeaderOneComponent } from './header/header-one/header-one.component';
import { HeaderThreeComponent } from './header/header-three/header-three.component';

@NgModule({
  declarations: [FooterComponent, NewsletterComponent, SocialtagComponent, ServiceComponent, GalleryComponent, InformationComponent, CopyrightsComponent, MenuComponent, TopbarComponent, HeaderSixComponent, HeaderTwoComponent, HeaderOneComponent, HeaderThreeComponent],
  
  imports: [
    CommonModule,
    RouterModule
  ],

  exports:
  [
    FooterComponent,
    HeaderSixComponent,
    HeaderTwoComponent,
    HeaderOneComponent,
    HeaderThreeComponent,
    MenuComponent
  ],
  providers:[
    WINDOW_PROVIDERS
  ]
  
})
export class SharedModule { }
